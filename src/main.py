from __future__ import annotations
from os import environ, rename
from pprint import pprint
from logger import get_logger
from Repo import Repo, Repo_Encoder
import pattern_finder
import concurrent.futures
import requests
import tempfile
import zipfile
import argparse
import json

logger = get_logger(__name__)

langs = {
    'rust': {
        'extension': 'rs',
        'regex': '(?m)(match\s+\w\s+\{(\s+\S+\s+=>\s.+,)+\s+\})'
    },
    'scala': {
        'extension': 'scala',
        'regex': '(?ms)((?:\S+|\w+)\s+match\s+\{(?:\{[^\}]*\}|[^\}])+\})'
        # 'regex': '(?ms)(match\s+\{.*\})'
    },
    'haskell': {
        'extension': 'hs',
        'regex': '(?ms)^[\ \t]*case\ [^\n]+ of\n?(([^\n]*\n?[^\n]*((->)|\|)[^\n]*)\n)+'
    },
    'ml': {
        'extension': 'ml',
        'regex': '(?ms)(case\s+\w+\s+of[^;]+;)'
    },
    'swift': {
        'extension': 'swift',
        'regex': '(?ms)(switch\s+\w+\s+\{(?:\{[^\}]*\}|[^\}])+\})'
    },
    'elixir': {
        'extension': 'ex',
        'regex': '(?ms)(case\s+\S+\s+((?!end).)*end)'
    },
    # 'ocaml': {
    #     'extension': 'ml',
    #     'regex': '(?ms)(match\s+\w+\s+with\s+((?!;;).)*\s+;{2})'
    # }
}
repos = []


def main(args: Namespace):
    """
    Download and extract github repo's for the requested languages
    """

    # Query API for repos for specific lang
    repos_by_lang = {}
    for lang in langs.keys():
        logger.info(f'Getting urls for {lang}')
        repo_infos: [dict] = get_repo_urls(lang=lang, num=args.repos_per_lang,
                                           github_token=args.github_token)
        repos_by_lang[lang] = []

        for repo_info in repo_infos:
            # Check if repo is too big before download
            size = int(repo_info['size'])
            if size > args.max_repo_size:
                logger.info(f'{repo_info["url"]} too big')
                continue

            repos_by_lang[lang].append(repo_info)

    # Download the queried repos asynchronously
    with concurrent.futures.ThreadPoolExecutor(max_workers=args.max_threads) as executor:
        for lang in langs.keys():
            for repo_info in repos_by_lang[lang]:
                executor.submit(
                    save_repo, url_info=repo_info, lang=lang, extract_loc=args.output_dir)
    # for lang in langs.keys():
    #     logger.info(f'Extracting repos for {lang}')
    #     for repo_info in repos_by_lang[lang]:
    #         save_repo(url_info=repo_info, lang=lang,
    #                   extract_loc=args.output_dir)

    for lang in langs.keys():
        repo_infos = repos_by_lang[lang]
        for repo_info in repo_infos:
            name = repo_info['name']
            path = f'{args.output_dir}{lang}/{name}'
            source_ext = langs[lang]['extension']
            repo = Repo(name, path, lang, source_ext)
            repos.append(repo)

    # Create results.json for writing
    f = open('results.csv', 'w')
    f.close()

    pattern_finder.analyse_repos(repos)


def get_repo_urls(lang: str, num: int, github_token: str) -> [{'url': str, 'name': str, 'size': str}]:
    """Returns a list of urls for repos that contain code with the requested language

    Args:
        lang (str): The language the search for
        num (int): The number of repo urls to return in the list

    Returns:
        [{
            'url' (str): The download url for the repo zip file
            'name' (str): Name of the repo
            'size' (str): The reported diskUsage of the repo
        }]: 
    """

    query_string = ('query repos {'
                    f'search(query: "language:{lang}", type: REPOSITORY, first: {num})' + '{'
                    'nodes {'
                    '... on Repository {'
                    'url\n'
                    'name\n'
                    'diskUsage}}}}')

    req_string = {
        'query': query_string,
        'variables': {},
        'operationName': 'repos'
    }

    res = requests.post('https://api.github.com/graphql', json=req_string, headers={
        'Authorization': f'bearer {github_token}'
    })

    logger.info(res.json())

    url_infos = [
        {
            'url': node['url'] + '/archive/master.zip',
            'name': node['name'],
            'size': node['diskUsage']
        }
        for node in res.json()['data']['search']['nodes']
    ]

    logger.info(url_infos)

    return url_infos


def save_repo(url_info: {'url': str, 'name': str, 'size': str}, lang: str,
              extract_loc: str) -> None:
    url = url_info['url']

    logger.info(f'Downloading {url}')
    res = requests.get(url)

    # Write the downloaded zipfile to the file system as a temporary file
    with tempfile.NamedTemporaryFile(mode='wb') as f:
        # with open(f"/tmp/test.zip", "wb") as f:
        f.write(res.content)

        # Extract the zipfile (repo) to the /tmp/test/{lang}/ directory
        with zipfile.ZipFile(f.name, 'r') as zip_ref:
            repo_name = url_info['name']
            outfile_name = f'{extract_loc}{lang}/{repo_name}'

            logger.info(f'Extracting {outfile_name}')

            zip_ref.extractall(f'{extract_loc}{lang}')
            extract_dir = f'{extract_loc}{lang}'
            rename(
                f'{extract_dir}/{list(zip_ref.NameToInfo)[0]}', f'{extract_dir}/{repo_name}')

            # logger.info(
            #     f'Renamed {extract_dir}/{list(zip_ref.NameToInfo)[0]} to {extract_dir}/{repo_name}')
            # logger.info(f'Extracted repo at: {outfile_name}')


def get_repos():
    return repos


def setup_args() -> Namespace:
    parser = argparse.ArgumentParser()

    repos_group = parser.add_mutually_exclusive_group(required=True)
    repos_group.add_argument(
        '--output-dir', type=str, help='The directory to save the repositories in')
    # TODO Make input-dir work
    repos_group.add_argument(
        '--input-dir', type=str, help='The directory that has saved repos from a previous run')

    parser.add_argument(
        'github_token', type=str, help='Your Github personal access token')
    parser.add_argument(
        '--repos-per-lang', type=int, default=1, help='The number of repositories to get per language')
    parser.add_argument(
        '--max-repo-size', type=int, default=1000000, help='The maximum repository size to extract')
    args = parser.add_argument(
        '--max-threads', type=int, default=3, help='The maximum number of threads used to download the repositories')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = setup_args()
    main(args)
