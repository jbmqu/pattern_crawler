from logger import get_logger
from json import JSONEncoder
import glob
import File

logger = get_logger(__name__)


class Repo():
    name: str
    url: str
    path: str
    lang: str
    source_extension: str
    source_files = [File]
    matched_files: [File]

    def __init__(self, name, path, lang, source_extension):
        self.name = name
        self.path = path
        self.lang = lang
        self.source_extension = source_extension
        self.source_files = self._get_source_files()

    def _get_source_files(self) -> [File]:
        source_files = glob.glob(
            f'{self.path}/**/*.{self.source_extension}', recursive=True)
        # logger.info(source_files)
        return source_files


class Repo_Encoder(JSONEncoder):
    def default(self, o):
        return o.__dict__
