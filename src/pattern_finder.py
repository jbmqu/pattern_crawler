from __future__ import annotations
from progress.bar import Bar
from logger import get_logger
from main import langs
from File import File
from Repo import Repo_Encoder
import re
import csv

logger = get_logger(__name__)


def analyse_repos(repos):
    for repo in repos:
        logger.info(
            f'Searching {repo.name}, {repo.lang}: {len(repo.source_files)} files')
        matched_files: [File] = find_pattern_matching(
            repo.source_files, langs[repo.lang]['regex'], repo.lang)

        if not matched_files:
            continue

        repo.matched_files = matched_files

        logger.info(
            f'Writing results to results.csv for repo {repo.name}: {repo.lang}')
        with open('results.csv', 'r+', newline='') as f:
            writer = csv.writer(f)
            for matched_file in repo.matched_files:
                for matched_lines in matched_file.matched_lines:
                    writer.writerow(
                        [repo.name, repo.lang, matched_file.name, matched_lines])

    return repos


def find_pattern_matching(file_paths: [str], regex: str, lang: str) -> [File]:
    r = re.compile(regex)
    matched_files = []

    file_name = ''
    bar = Bar('Searching', max=len(file_paths))
    for file_path in file_paths:
        file_name = file_path.split('/')[-1]
        bar.suffix = f'%(index)d/%(max)d [%(eta_td)s] {file_name}' % bar

        with open(file_path, 'r') as f:
            lines = f.read()
            matches_for_file = []

            for match in r.finditer(lines):
                matches_for_file.append(match.group(0))

                # logger.info(f'{file_path}')
                # logger.info(match.group(0))

            if matches_for_file:
                matched_file = File(file_name, file_path,
                                    lang, matches_for_file)
                matched_files.append(matched_file)

        bar.next()
    bar.finish()
    logger.info(f'\n\n')

    return matched_files
