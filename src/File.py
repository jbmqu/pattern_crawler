

class File:
    name: str
    path: str
    lang: str
    matched_lines = [int]

    def __init__(self, name, path, lang, matched_lines):
        self.name = name
        self.path = path
        self.lang = lang
        self.matched_lines = matched_lines
