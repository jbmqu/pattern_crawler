# Pattern Crawler
A python program to scrape github for repositories that may contain pattern matching code for various languages

## Quickstart
* Obtain a [github personal access token](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token)
* Run with `python ./src/main.py <download_dir> <github_token>`
